#include "tests.h"

namespace tst {

    double freq_test(const sequence& epsilon) {
        if(is_empty(epsilon)) {
            return 0;
        }
        auto sum_lambda = [](long sum, char curr){ return sum + (2 * curr - 1); };
        long sum = std::abs(std::accumulate(epsilon.begin(), epsilon.end(),
                                            0, sum_lambda));
        return (double)sum / epsilon.size();
    }


    double block_freq_test(const sequence& epsilon) {
        if(is_empty(epsilon)) {
            return 0;
        }
        double p = std::accumulate(epsilon.begin(), epsilon.end(), 0.0) /
                   epsilon.size();
        /* Вычисление параметра V через скалярный продукт двух векторов.
         * Первый вектор - входная последовательность бит от первого до n-1
         * В качестве второго вектора выступает та же последовательность
         * epsilon, но смещеннаая на 1 от начала
         */
        int v = std::inner_product(epsilon.begin(), epsilon.end() - 1,
                                   epsilon.begin() + 1,
                                   1, std::plus<>(), std::not_equal_to<>());
        size_t n = epsilon.size();
        return std::abs(v - 2 * n * p * (1 - p)) /
              (2 * sqrt2 * sqrt(n) * p * (1 - p));
    }

    const std::vector<double> random_deviation_test(const sequence& epsilon) {
        std::vector<double> metrics(18);
        if(is_empty(epsilon)) {
            return metrics;
        }
        sequence x(epsilon.size());
        std::transform(epsilon.begin(), epsilon.end(), x.begin(),
                       [](char ei) -> char{ return 2 * ei - 1; });
        sequence s(epsilon.size() + 2);     //вектор частичных сум + рамка из нулей
        std::partial_sum(x.begin(), x.end(), s.begin() + 1);
        int l = std::count(s.begin(), s.end(), 0) - 1;
        std::map<int, int> ksi;
        for(const auto si : s) {
           ksi[si]++;
        }
        int j = -9;
        for(auto& metric : metrics) {
            metric = std::abs(ksi[j] -l) /
                    std::sqrt(2 * l * (4 * abs(j) - 2));
            j == -1 ? j += 2 : j++; //пропуск j == 0
        }
        return metrics;
    }

    static bool is_empty(const sequence& epsilon) {
        return !epsilon.size();
    }

}
