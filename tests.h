#ifndef TESTS_H
#define TESTS_H
#include <vector>
#include <map>
#include <cstdint>
#include <numeric>
#include <functional>
#include <algorithm>
#include <cmath>
#include <numbers>
#include <iostream>
#include "fmt/format.h"

using namespace std::numbers;
using sequence = std::vector<int8_t>;

namespace tst {
    /**
     * Шаблон процедуры генерации последовательности целочисленных
     * элементов IntType генератором G
     */
    template<class G, typename IntType>
    void generate(sequence& epsilon,
                  G gen,
                  std::uniform_int_distribution<IntType> uid) {
        fmt::print("Enter vector length: ");
        long epsilon_size = 0;
        std::cin >> epsilon_size;

        epsilon.resize(epsilon_size);
        std::generate(epsilon.begin(), epsilon.end(), [&](){ return uid(gen); });

        fmt::print("Vector generated\n");
}

    /**
      * Пороговое значение тестов при котором тест считается успешно пройденным
      */
    inline constexpr double test_limit { 1.82138636 };

    /**
     * @brief freq_test Вычисляет значение частотного теста NIST
     * @param epsilon входная последовательность бит
     * @return числовое значение теста
     */
    double freq_test(const sequence& epsilon);

    /**
     * @brief block_freq_test Вычисляет значение теста на
     * последовательность одинаковых бит
     * @param epsilon входная последовательность бит
     * @return числовое значение теста
     */
    double block_freq_test(const sequence& epsilon);

    /**
     * @brief random_deviation_test Вычисляет значение расширенного теста
     * на произвольные отклонения
     * @param epsilon входная последовательность бит
     * @return вектор из 18 статистик
     */
    const std::vector<double> random_deviation_test(const sequence& epsilon);

    /**
     * @brief is_empty проверяет длину последовательности
     * @param epsilon проверяемая последовательность бит
     * @return Истина если пустая последовательность, иначе ложь
     */
    static bool is_empty(const sequence& epsilon);
}
#endif // TESTS_H
