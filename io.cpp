#include "io.h"

void print_color(const std::string& text, int16_t color) {
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, color);
    fmt::print("{}", text);
    SetConsoleTextAttribute(handle, 15); //15 - default black foreground
}


void print_conclusion(double test_result) {
    if(test_result < tst::test_limit) {
        print_color("SUCCESS\n", FOREGROUND_GREEN);
    } else {
        print_color("FAILED\n", FOREGROUND_RED);
    }
}

void print_result(const std::string& test_name, double test_result) {
    fmt::print("{:<20} : {:.5f} ", test_name + " result ", test_result);
    print_conclusion(test_result);
}



void print_d_test(const std::vector<double>& rd_metrics) {
    for(int i = -9; i < 10; i == -1 ? i += 2 : i++) {
        fmt::print("#{:2d} metric ", i);
        print_result("deviation test", rd_metrics[i + 9]);
    }
}

void output(std::ostream& ostream, const sequence& eps,
            char const* delimiter) {
    std::copy(eps.begin(), eps.end(),
              std::ostream_iterator<int>(ostream, delimiter));
    ostream << "\n";
}

void outToFile(const sequence& eps) {
    fmt::print("Enter out file :");
    std::string outfile;
    std::cin >> outfile;
    std::ofstream fout(outfile);
    if(fout.is_open()) {
        output(fout, eps, " ");
        fmt::print("Epsilon wrote in file\n");
    } else {
        fmt::print("Writing failed!\n");
    }
}


void loadFromFile(sequence& eps) {
    fmt::print("Enter input file :");
    std::string infile;
    std::cin >> infile;
    std::ifstream fin(infile);
    if(fin.is_open()) {
        eps.clear();
        std::copy_if(std::istream_iterator<int>(fin),
                     std::istream_iterator<int>(),
                     std::inserter(eps, eps.end()),
                     [](int8_t i) { return  i >= 0 && i < 2; });
        fmt::print("Epsilon load from file\n");
    } else {
        fmt::print("Loading failed!\n");
    }

}
