#ifndef OUTPUT_H
#define OUTPUT_H
#include <vector>
#include <fstream>
#include <iostream>
#include <windows.h>

#include "fmt/core.h"
#include "fmt/color.h"
#include "tests.h"
/**
 * @brief print_color печатает строку соотвествующим цветом в терминале
 * @param text строка для вывода в терминал
 * @param color значение ASCII кода цвета
 */
static void print_color(const std::string& text, int16_t color);
/**
 * @brief print_conclusion выводит в консоль заключение по тесту
 * (пройден или нет)
 * @param test_result числовое значение теста
 */
static void print_conclusion(double test_result);

/**
 * @brief print_result выводит в консоль результат теста и заключение о
 * том пройден он тестовой выборкой или нет
 * @param test_name имя теста
 * @param test_result числовой результат теста
 */
void print_result(const std::string& test_name, double test_result);

/**
 * @brief print_d_test  Печатает в терминале серию тестов на отклонения
 * @param rd_metrics вектор результатов тестов
 */
void print_d_test(const std::vector<double>& rd_metrics);

/**
 * @brief output копирует последовательность в выходной поток
 * @param ostream   выходной поток
 * @param eps       последовательность
 * @param delimiter разделитель элементов
 */
void output(std::ostream& ostream, const sequence& eps,
            char const* delimiter);

/**
 * @brief outToFile процедура вывода последовательности
 * в текстовый файл
 * @param eps последовательность для вывода
 */
void outToFile(const sequence& eps);

/**
 * @brief loadFromFile процедура загрузки последовательности
 * из текстового файла
 * @param eps контейнер для сохранения
 */
void loadFromFile(sequence& eps);

#endif // OUTPUT_H
