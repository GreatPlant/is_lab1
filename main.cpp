#include <iostream>
#include <random>

#include "fmt/core.h"
#include "fmt/color.h"
#include "tests.h"
#include "io.h"


int main()
{
    std::mt19937 gen { std::random_device()() };
    std::uniform_int_distribution<int8_t> uid(0, 1);
    sequence epsilon;
    int choosen = 0;
    do {
        fmt::print("Choose function:\n"
                        "\tGenerate: 1\t\t"         "Load from file: 2\n"
                        "\tOut on screen: 3\t"      "Out to file: 4\n"
                        "\tFreq test: 5\t\t"        "Block test: 6\n"
                        "\tDeviation test: 7\t"     "All tests: 8\n"
                   "Enter 0 for exit\n"
                   "Your choose: ");
        std::cin >> choosen;
        switch (choosen) {
            case 1:
                gen.seed(std::random_device()());
                tst::generate<std::mt19937, int8_t>(epsilon, gen, uid);
                break;
            case 2:
                loadFromFile(epsilon);
                break;
            case 3:
                output(std::cout, epsilon, " ");
                break;
            case 4:
                outToFile(epsilon);
                break;
            case 5:
                print_result("Freq test", tst::freq_test(epsilon));
                break;
            case 6:
                print_result("Block test", tst::block_freq_test(epsilon));
                break;
            case 7:
                print_d_test(tst::random_deviation_test(epsilon));
                break;
            case 8:
                print_result("Freq test", tst::freq_test(epsilon));
                print_result("Block test", tst::block_freq_test(epsilon));
                print_d_test(tst::random_deviation_test(epsilon));
                break;
            case 0:
                fmt::print("EXIT\n");
                break;
            default:
                fmt::print("Wrong! Try again\n");
        }
    } while(choosen);
    return 0;
}
